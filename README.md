   CONSIDERATIONS FOR SELECTING STRUCTS
   
   ### Array of Structs: 
    
       The primary data structure used in this code is an array of structs (struct Student). This   data structure is suitable for representing a collection of student records, where each record contains multiple fields such as name, date of birth, registration number, program code, and annual tuition fees. Using an array allows us to store multiple student records contiguously in memory, simplifying access and manipulation.

    ### Dynamic Memory Allocation: 
       The code dynamically allocates memory for the array of Student structs using malloc. This is done to accommodate a variable number of students based on user input (n). Dynamic memory allocation allows flexibility in memory usage, enabling the program to adapt to different numbers of students without wasting memory.

    ###Pointer to Float: 
       The annual_tuition_fees field in the Student struct is declared as a pointer to float (float *annual_tuition_fees). This design choice is made to allow each student's annual tuition fees to be dynamically allocated, ensuring efficient memory usage and flexibility in storing fees of varying precision.

    ###String Storage: 
       String fields such as name, date_of_birth, reg_no, and program_code are stored as character arrays within the Student struct. The choice of character arrays allows for storing variable-length strings, with a maximum specified length for each field (name has a maximum length of 50 characters, date_of_birth has 10 characters, reg_no has 6 characters, and program_code has 4 characters).

    ###Error Handling: 
       The code includes error handling mechanisms to validate user input for fields such as name length, date format, registration number format, program code format, and annual tuition fees. These checks ensure that the data stored in the data structure adheres to specified constraints and prevents unexpected behavior or memory corruption.

Name                           youtube video link                  registration number
KAHUNDE AUDREY              https://youtu.be/2e7-DfgGPbc            23/U/08649/EVE        

SSEBWANA JOHN PAUL          https://youtu.be/bIlqCUYV7-4            23/U/17415/PS

RWOTHOMIO JONATHAN          https://youtu.be/BK3DIQrHu7w            23/U/17155/PS
