#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <ctype.h>

// Declaring a structure Student
struct Student {
    char name[51];
    char date_of_birth[11];
    char reg_no[7];
    char program_code[5]; // Increase size to accommodate null terminator
    float *annual_tuition_fees;
};

// Function prototypes
void printMenu();

// Function to create a new student
void createStudent(struct Student students[], int *num_students) {
    if (*num_students < 100) { // Check against maximum size
        struct Student new_student;
        printf("Enter student name: ");
        scanf("%s", new_student.name);
        printf("Enter the date of birth (YYYY-MM-DD): ");
        scanf("%s", new_student.date_of_birth);
        printf("Enter registration number: ");
        scanf("%s", new_student.reg_no);
        printf("Enter the program code: ");
        scanf("%s", new_student.program_code);
        printf("Enter the annual tuition: ");
        scanf("%f", &new_student.annual_tuition_fees);

        students[*num_students] = new_student;
        (*num_students)++;
        printf("Student created successfully.\n");
    } else {
        printf("Maximum number of students reached.\n");
    }
}

// Function for reading students details
void readStudent(struct Student students[], int num_students) {
    printf("Student Details:\n");
    for (int i = 0; i < num_students; i++) {
        printf("Student %d\n", i + 1);
        printf("Name: %s\n", students[i].name);
        printf("Date of Birth: %s\n", students[i].date_of_birth);
        printf("Registration Number: %s\n", students[i].reg_no);
        printf("Program Code: %s\n", students[i].program_code);
        printf("Annual Tuition: %.2f\n", *(students[i].annual_tuition_fees));
        printf("\n");
    }
}


void updateStudent(struct Student students[], int num_students);
void deleteStudent(struct Student students[], int *num_students);
void searchStudent(struct Student students[], int num_students);
void sortStudents(struct Student students[], int num_students);
void exportToCSV(struct Student students[], int num_students);

int main() {
    int n;
    printf("Enter the number of students: ");
    scanf("%d", &n);

    // Allocate memory for the array of students
    struct Student *students = (struct Student *)malloc(n * sizeof(struct Student));
    // Check if memory allocation was successful
    if (students == NULL) {
        printf("Memory allocation failed!");
        return 1;
    }

    // Input student details
    for (int i = 0; i < n; i++) {
        printf("Enter the details of student %d:\n", i + 1);
        printf("Name: ");
        scanf("%50s", students[i].name);
        // Check if name is not more than 50 characters
        if (strlen(students[i].name) > 50) {
            printf("Error: Name should not be more than 50 characters long\n");
            return 1;
        }

        // Input and validate date of birth
        printf("Date of birth (YYYY-MM-DD): ");
        scanf("%10s", students[i].date_of_birth);
        if (strlen(students[i].date_of_birth) != 10 || students[i].date_of_birth[4] != '-' || students[i].date_of_birth[7] != '-') {
            printf("Error: Date of birth should be in the format YYYY-MM-DD\n");
            return 1;
        }

        // Input and validate registration number
        printf("Registration number(eg.123456): ");
        scanf("%6s", students[i].reg_no);
        if (strlen(students[i].reg_no) != 6) {
            printf("Error: Registration number should be exactly 6 characters\n");
            return 1;
        }

        // Check if registration number is not more than 6 digits
        for (int j = 0; j < 6; j++) {
            if (!isdigit(students[i].reg_no[j])) {
                printf("Error: Registration number should not be more than 7 digits\n");
                return 1;
            }
        }

        // Input and validate program code
        printf("Program code(eg. BCSC): ");
        scanf("%4s", students[i].program_code);
        if (strlen(students[i].program_code) != 4) {
            printf("Error: Program code should be exactly 4 characters\n");
            return 1;
        }

        // Input and validate annual tuition fees
        printf("Annual tuition fees: ");
        students[i].annual_tuition_fees = (float *)malloc(sizeof(float));
        if (students[i].annual_tuition_fees == NULL) {
            printf("Memory allocation failed!\n");
            return 1;
        }
        scanf("%f", students[i].annual_tuition_fees);
        if (*(students[i].annual_tuition_fees) == 0.0f) {
            printf("Error: Annual tuition fees cannot be zero!\n");
            return 1;
        }
    }

    int choice = 0;

    // Menu loop
    while (choice != 8) {
        printMenu();
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                createStudent(students, &n);
                break;
            case 2:
                readStudent(students, n);
                break;
            case 3:
                updateStudent(students, n);
                break;
            case 4:
                deleteStudent(students, &n);
                break;
            case 5:
                searchStudent(students, n);
                break;
            case 6:
                sortStudents(students, n);
                break;
            case 7:
                exportToCSV(students, n);
                break;
            case 8:
                printf("Exit\n");
                break;
            default:
                printf("Invalid choice. Please enter a number between 1 and 8.\n");
        }
    }

    // Free memory for annual tuition fees for each student
    for (int i = 0; i < n; i++) {
        free(students[i].annual_tuition_fees);
    }

    // Free memory allocated for the array of students
    free(students);

    return 0;
}

// Function to print menu options
void printMenu() {
    printf("\nMenu Options:\n");
    printf("1. Create Student\n");
    printf("2. Read Students\n");
    printf("3. Update Students\n");
    printf("4. Delete Students\n");
    printf("5. Search Student by Registration Number\n");
    printf("6. Sort Students\n");
    printf("7. Export Records to CSV\n");
    printf("8. Exit\n");
}

// Function to create a new student
void createStudent(struct Student students[], int *num_students) {
    if (*num_students < 100) { // Check against maximum size
        struct Student new_student;
        printf("Enter student name: ");
        scanf(" %50[^\n]", new_student.name);;
        printf("Enter the date of birth (YYYY-MM-DD): ");
        scanf(" %10[^\n]", new_student.date_of_birth);
        printf("Enter registration number: ");
        scanf(" %6[^\n]", new_student.reg_no);
        printf("Enter the program code: ");
        scanf(" %4[^\n]", new_student.program_code);
        printf("Enter the annual tuition: ");
        scanf("%f", &new_student.annual_tuition_fees);

        students[*num_students] = new_student;
        (*num_students)++;
        printf("Student created successfully.\n");
    } else {
        printf("Maximum number of students reached.\n");
    }
}

// Function for reading students details
void readStudent(struct Student students[], int num_students) {
    printf("Student Details:\n");
    for (int i = 0; i < num_students; i++) {
        printf("Student %d\n", i + 1);
        printf("Name: %s\n", students[i].name);
        printf("Date of Birth: %s\n", students[i].date_of_birth);
        printf("Registration Number: %s\n", students[i].reg_no);
        printf("Program Code: %s\n", students[i].program_code);
        printf("Annual Tuition: %.2f\n", *(students[i].annual_tuition_fees));
        printf("\n");
    }
}

// Function to update students information
void updateStudent(struct Student students[], int num_students) {
    char reg_no[8];
    printf("Enter the registration number of the student to update: ");
    scanf("%s", reg_no);
    for (int i = 0; i < num_students; i++) {
        if (strcmp(students[i].reg_no, reg_no) == 0) {
            printf("Enter student name: ");
            scanf(" %50[^\n]", students[i].name);
            printf("Enter the date of birth (YYYY-MM-DD): ");
            scanf(" %10[^\n]", students[i].date_of_birth);
            printf("Enter registration number: ");
            scanf(" %6[^\n]", students[i].reg_no);
            printf("Enter the program code: ");
            scanf(" %4[^\n]", students[i].program_code);
            printf("Enter the annual tuition: ");
            scanf("%f", &students[i].annual_tuition_fees);
            printf("Student updated successfully.\n");
            return;
        }
    }
    printf("Student not found.\n");
}

// Function for deleting students from the system
void deleteStudent(struct Student students[], int *num_students) {
    char reg_no[7];
    printf("Enter the registration number of the student to delete: ");
    scanf("%s", reg_no);
    for (int i = 0; i < *num_students; i++) {
        if (strcmp(students[i].reg_no, reg_no) == 0) {
            for (int j = i; j < *num_students - 1; j++) {
                students[j] = students[j + 1];
            }
            (*num_students)--;
            printf("Student deleted successfully.\n");
            return;
        }
    }
    printf("Student not found.\n");
}

// Function to search a student by their registration number
void searchStudent(struct Student students[], int num_students) {
    char reg_no[8];
    printf("Enter the registration number of the student to search: ");
    scanf("%7s", reg_no); // Limit input to 7 characters to avoid buffer overflow
    for (int i = 0; i < num_students; i++) {
        if (strcmp(students[i].reg_no, reg_no) == 0) {
            printf("Student found:\n");
            printf("Name: %s\n", students[i].name);
            printf("Date of Birth: %s\n", students[i].date_of_birth);
            printf("Registration Number: %s\n", students[i].reg_no);
            printf("Program Code: %s\n", students[i].program_code);
            printf("Annual Tuition: %.2f\n", *(students[i].annual_tuition_fees));
            return;
        }
    }
    printf("Student not found.\n");
}

// Function to sort students based on selected field
void sortStudents(struct Student students[], int num_students) {
    // Submenu to prompt the user for sorting field
    printf("Select sorting field:\n");
    printf("1. Name\n");
    printf("2. Date of Birth\n");
    printf("3. Registration Number\n");
    printf("4. Program Code\n");
    int choice;
    printf("Enter your choice: ");
    scanf("%d", &choice);

    switch (choice) {
        case 1:
            // Sort by name using Bubble Sort
            for (int i = 0; i < num_students - 1; i++) {
                for (int j = 0; j < num_students - i - 1; j++) {
                    if (strcmp(students[j].name, students[j + 1].name) > 0) {
                        // Swap students
                        struct Student temp = students[j];
                        students[j] = students[j + 1];
                        students[j + 1] = temp;
                    }
                }
            }
            break;
        // You can implement sorting by other fields similarly
        case 2:
            // Sort by date of birth
            // Implement sorting algorithm here
            break;
        case 3:
            // Sort by registration number
            // Implement sorting algorithm here
            break;
        case 4:
            // Sort by program code
            // Implement sorting algorithm here
            break;
        default:
            printf("Invalid choice.\n");
            return;
    }

    // Print sorted list
    printf("Sorted list:\n");
    for (int i = 0; i < num_students; i++) {
        printf("[%d] %s\n", i + 1, students[i].name); // Print the selected field
    }
}

void exportToCSV(struct Student students[], int num_students) {
    FILE *fp;
    char fileName[100]; // Assuming a maximum file name length of 100 characters
    printf("Enter the name of the CSV file to export to: ");
    scanf(" %99[^\n]", fileName);

    fp = fopen(fileName, "w"); // Open file in write mode (overwrite existing content)
    if (fp == NULL) {
        printf("Error opening file!\n");
        return;
    }

    fprintf(fp, "Name,Date of Birth,Registration Number,Program Code,Annual Tuition\n");
    for (int i = 0; i < num_students; i++) {
        fprintf(fp, "%s,%s,%s,%s,%.2f\n", students[i].name, students[i].date_of_birth, students[i].reg_no, students[i].program_code, *(students[i].annual_tuition_fees));
    }

    fclose(fp);
    printf("Data exported to %s successfully!\n", fileName);
}
